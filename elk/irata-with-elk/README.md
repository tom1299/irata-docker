# IRATA with ELK Stack
This folder contains a docker swarm based setup with the `IRATA` auction-service
stack deployed alongside an ELK-Stack

Actually its not a complete elk stack but rather an `Elasticsearch` with a
`Kibana` without `Logstash`

## Purpose
The purpose of this setup is to prove that the `Ingress load balancer` balances
requests to the different instances of the auciton-service. The procedure
for proving that looks like this:

* Configure the `filbeat` based docker image of the auction-service to send
log events to `Elasticsearch` on the elk stack using a predefined pipeline for parsing Spring-Boot-Log-Events
* Deploy the elk stack
* Add above mentioned pipeline to Elasticsearch after deployment of the stack
* Deploy the irata auction service stack with 2 instances of the service
* See whether log messages are visible in Kibana
* Run a test scenario
* Query the log index to find out whether requests have been distributed among the two service instances (`hostname` property)

### Configuring the auction-service image

## Current state

* `02.06.2018-0`: Currently it is not possible to use defaults for pipelines on an index. See [here]https://discuss.elastic.co/t/default-ingest-pipeline-for-index/77647). Until this feature has been implemented using logstash as
an intermediary component between filebeat and elasticsearch is necessary
* `02.06.2018-1`: The docker-compose file not contains a full blown elk statck with
logstash taken from this really good template [here](https://github.com/deviantony/docker-elk)
  + Next step: Configure logstash to use input from filebeat and use a specific index with the grok pattern previously part of the elasticsearch pipeline
* `02.06.2018-2`: Above mention docker-compose file uses the build context for building the images. Since I needed to change the logstash configuration so that two additional plugins are installed and I was working behind a proxy this did not work anymore. See [here](https://github.com/mattermost/mattermost-docker/issues/74)
* `02.06.2018-3`: Adding support for filebeat and grok did work then with no problems
* `16.06.2018`: The filebeat on the auction service can not commmunicate with logstash on port 5044. Ping works. Telnet not.
In the logstash image itself telnet works
* `16.06.2018-2`: Problems seems to be related to the fact that the port `5044` is not published.
* `16.06.2018-3`: Adding port publication did not solve the problem. In addition telneting `elasticsearch` on port 9200 does not work either
* `17.06.2018`: Telneting the port from `elasticsearch` from the same stack works indicating that its related to _intert-stack_ commmunication
* `17.06.2018-2`: Indeed `nc` does not work from irata stack. Next: Check whether related to OS of logstash and elasticsearch by deploying a simple httpd
* `17.06.2018-3`: When deploying a simple `httpd` on the elk stack and using `nc` to do a htttp-get works:

```
/ # nc -v elk_httpd 80
elk_httpd (10.0.0.6:80) open
GET /index.html HTTP/1.1\r\nHost:10.0.0.10\r\n\r\n
HTTP/1.1 400 Bad Request
Date: Sat, 16 Jun 2018 13:20:49 GMT
Server: Apache/2.4.33 (Unix)
Content-Length: 226
Connection: close
Content-Type: text/html; charset=iso-8859-1

<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>400 Bad Request</title>
</head><body>
<h1>Bad Request</h1>
<p>Your browser sent a request that this server could not understand.<br />
</p>
</body></html>
```
* Next: Use `expose` in docker-compose file to expose ports
* `17.06.2018-4`: Exposing the ports in the docker file as well as in the docker-compose file did not work either. Nevertheless: Telneting from Kibana to Logstash works...
* `17.06.2018-5`: Added httpd to elk stack for testing. And the httpd can not be accessed with the port from irata. Strange...
* `18.06.2018-1`: I deploy everything in one stack but still it is not working. It seems that only the services can access each others ports who either all run on `workers` or `manager`... Next: Deploy everything on the manager node
* `18.06.2018-2`: When deploying everything on the manager node. It works
* `19.06.2018-1`: Same is true when deploying on workers only
* `19.06.2018-2`: Messages arrive at elastic serach finally. Logstash grok pattern does not seem to work. The message is not decomposed

## Summary of connection problem
During the first tries it was not possible to connect from the irata auction service to logstash (see above).
The problem was solved by deploying all services to `workers` instead of mixed `workers` and `managers`.
