#!/bin/sh
set -e

cd /

# Start filebeat
filebeat -c /filebeat.yml -d "publish" &
