# ELK Stack for irata
In this file the elk stack for the irata application is defined

##
Deploy the stack

	docker stack deploy --compose-file docker-compose.yml irata-elk

## Links
Troubleshoot non starting docker services

	https://stackoverflow.com/questions/45372848/docker-swarm-how-to-find-out-why-service-cant-start


## Next steps
Connect the stack with the filebeat/sprung-boot acution-service to the elk-stack so that log messages can be viewed using kibana
