# Logstash example
This folder contains a simple logstash image and a sample docker-compose for usage with elastic search.

The main responsibility of the logstash component would have been to parse the log messages of the services using `grok`.

## Current status
Originally i intended to use logstash with filebeat as input in combination with an alpine logstash image.

Unfortunately that did not work for several reasons. The main reason being that the alpine images for logstash are deprecated.

So I am now using a full fletched centos logstash.

## Future
As my intention was to keep image sizes for monitoring images as small as possible I have abandoned the idea of using logstash.

Instead I will try to use `elasticsearch` directly with filebeat and a grok processor in front of elastic serach.

See [here](https://www.elastic.co/blog/new-way-to-ingest-part-1) for the grok processor.
