# This folder contains a simple elk stack
## Purpose

* Expermient with piplines
* The overall goal is to define a pipline for the filebeat index that uses a grok pattern to process the messages from a microservice based on spring boot

## Steps
The following steps / tasks need to be done in order to achieve this goal:

* Find out how to create a pipeline in elasticsearch
* Find out how to associate a pipeline with an index / message
* Create a pipeline using a `grok` processor to parse log messages in spring boot format

## Creating a a test stack
In order to facilitate the development work I added a simple stack containing elasticsearch and kibana:

```YAML
version: '3.1'

services:

  elasticsearch:
    image: elasticsearch

  kibana:
    image: kibana
    ports:
      - 5601:5601

```

When the stack is running I simple can use kibana by using the URL [http://127.0.0.1:5601](http://127.0.0.1:5601).

This enables me to sue the [Console Dev-Tool](https://www.elastic.co/guide/en/kibana/6.x/console-kibana.html) of Kibana which makes it easier to do some tests with the ELK stack.

### Create a simple pipeline
Creating a pipeline is actually quiet simple as explained [here](https://www.elastic.co/guide/en/elasticsearch/reference/current/put-pipeline-api.html). I can be done using a simple curl:

```Bash
curl -X PUT "localhost:9200/_ingest/pipeline/my-pipeline-id" -H 'Content-Type: application/json' -d'
{
  "description" : "describe pipeline",
  "processors" : [
    {
      "set" : {
        "field": "foo",
        "value": "bar"
      }
    }
  ]
}
'
```
This will create a pipeline with the id `my_pipeline_id`. In this case the processor will replace the values `foo` with `bar`. So not very sophisticated but for an example that's okay.

### Use the pipeline for an index
Let's try to use the pipeline when posting data to an index.
First we will just use the simple `twitter` example without a pipeline being used.

For the first put we will not use the pipeline:

```Bash
PUT /twitter/doc/1?pretty
{
  "name": "Document without replacement",
  "foo": "should not be replaced"
}
```

For the second put we will append the pipeline:

```Bash
PUT /twitter/doc/2?pretty&pipeline=my-pipeline-id
{
  "name": "Document replacement",
  "foo": "should not be replaced with bar"
}
```

After these two puts we should have two documents in the index. The one with the id `2` should contain the value `bar` for the field `foo`.

Let's get all the documents from the index:

``` Bash
GET twitter/_search
```

```Json
{
  "took": 1,
  "timed_out": false,
  "_shards": {
    "total": 5,
    "successful": 5,
    "skipped": 0,
    "failed": 0
  },
  "hits": {
    "total": 2,
    "max_score": 1,
    "hits": [
      {
        "_index": "twitter",
        "_type": "doc",
        "_id": "2",
        "_score": 1,
        "_source": {
          "name": "Document replacement",
          "foo": "bar"
        }
      },
      {
        "_index": "twitter",
        "_type": "doc",
        "_id": "1",
        "_score": 1,
        "_source": {
          "name": "Document without replacement",
          "foo": "should not be replaced"
        }
      }
    ]
  }
}
```

Et voilà: The document with the id `2` indeed does contain the replaced value.

Now that we know how to add a processor, lets define a [grok processor](https://www.elastic.co/guide/en/elasticsearch/reference/current/grok-processor.html) next.

### Create a grok processor pipeline
Sine the overall goal is to use the pipeline with log messages from spring boot, let's create a grok processor with a pattern for that log format:

```Bash
PUT /_ingest/pipeline/spring-boot-pipeline
{
  "description" : "Pipeline for processing spring boot log messages",
  "processors": [
    {
      "grok": {
        "field": "message",
        "patterns": ["(?<timestamp>%{YEAR}-%{MONTHNUM}-%{MONTHDAY} %{TIME})  %{LOGLEVEL:level} %{NUMBER:pid} --- \\[(?<thread>[A-Za-z0-9-]+)\\] [A-Za-z0-9.]*\\.(?<class>[A-Za-z0-9#_]+)\\s*:\\s+(?<logmessage>.*)"]
      }
    }
  ]
}
```
So the pattern is not yet very sophisticated but for a start this is enough. Note that he backslashes need to be escaped when creating the processor with `PUT`.

Let's see how it works by posting a message:

```Bash
PUT /auction-service/doc/?pretty&pipeline=spring-boot-pipeline
{
  "message": "2018-05-28 09:18:08.441  INFO 8669 --- [main] com.jeeatwork.mc.model.AuctionService    : Created auction for good \"food\""
}
```

This should now have created a new index called `auction-service` and use the grok processor.

After successful completion, let's look at the documents in that index:

```Bash
GET auction-service/_search
```

```Json
{
  "took": 0,
  "timed_out": false,
  "_shards": {
    "total": 5,
    "successful": 5,
    "skipped": 0,
    "failed": 0
  },
  "hits": {
    "total": 1,
    "max_score": 1,
    "hits": [
      {
        "_index": "auction-service",
        "_type": "doc",
        "_id": "1",
        "_score": 1,
        "_source": {
          "level": "INFO",
          "logmessage": """Created auction for good "food"""",
          "pid": "8669",
          "thread": "main",
          "message": """2018-05-28 09:18:08.441  INFO 8669 --- [main] com.jeeatwork.mc.model.AuctionService    : Created auction for good "food"""",
          "class": "AuctionService",
          "timestamp": "2018-05-28 09:18:08.441"
        }
      }
    ]
  }
}
```

As we can see the fields have been created and we will now be able to query them. The pattern also needs some refining. The multiple double quotes are not necessary. Also the message field could also be removed.

## Next Steps

* Make the configuration of elasticsearch persistent
* Use the processor with file beat and the auction service
