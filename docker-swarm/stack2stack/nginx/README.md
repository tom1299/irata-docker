# Demo for inter-stack communication
This folder contains a very simple example for inter-stack communication between to stacks in a [docker swarm](https://docs.docker.com/engine/swarm/).

## Objectives

* Find out how services on two stacks in the same swarm can communicate
* Test whether and how replicated services are `load balanced`

## Approach

* First create an [overlay network](https://docs.docker.com/network/overlay/) called `core-net` which the two stacks can use to communicate
* Create two [docker compose](https://docs.docker.com/compose/overview/) files one for a stack called `frontend` and one for a stack called `backend`. The backend stack contains a simple nginx service which just returns the hostname of the container it is running in
* Deploy the two stacks
* Call the backend nginx service from one of the frontend services
* Scale the backend nginx service
* Call the backend service again

Expected result: First of all the backend service should be reachable by name (dns working). In addition after scaling the hostnames returned by the nginx service should vary, indicating the requests are _load balanced_

## Course of action

### Create the overlay network
This is pretty simple. The following command creates a network that can be used in a swarm

```shell
docker network create -d overlay --attachable core-net
```

### Deploy the frontend stack
The frontend stack in this case serves no special purpose except for testing the connectivity. The compose file looks like this:

```yaml
version: '3'

services:

  web-server:
    image: alpine
    command: tail -f /dev/null
    networks:
      - core-net
    deploy:
      placement:
        constraints:
          - node.role == worker

networks:
  core-net:
    external: true
```

The only service is called `web-server` and consists of an [alpine](https://hub.docker.com/_/alpine/) image. The somewhat peculiar `command` just prevents the image from shutting down directly after start.

The configured network references the just create one and is in addition part of the `web-server` service

The stack can be deployed like this:

```shell
docker stack deploy --compose-file docker-compose-frontend.yml frontend
```

This command creates a stack called `frontend`.

### Deploy the backend stack
The backend stack contains one service that is a simple nginx server. The server is configured to return the hostname when called. The compose file for the stack looks like this:

```yaml
version: '3'

services:

  web-server:
    image: registry.gitlab.com/tom1299/irata-docker/nginx-swarm
    ports:
      - "8888:80"
    networks:
      - core-net
    deploy:
      placement:
        constraints:
          - node.role == worker

networks:
  core-net:
    external: true
```

The only service also called `web-server` uses the same network as the service in the frontend stack

The stack can be deployed like this:

```shell
docker stack deploy --compose-file docker-compose-backend.yml backend
```

### Ping the backend
The first test is to ping the backend service from the frontend service. To do so I first look on which swarm worker the frontend service has been deployed. Then I log into that node and log into the alpine image running on the node.

In order to ping a service on a different stack a special name has to be used consisting of _stackname_ plus _service-name_. In this example the hostname to ping would be `backend_web-server`.

The ping ouput looks like this:

```shell
# ping backend_web-server
PING backend_web-server (10.0.0.77): 56 data bytes
64 bytes from 10.0.0.77: seq=0 ttl=64 time=0.147 ms
64 bytes from 10.0.0.77: seq=1 ttl=64 time=0.138 ms
64 bytes from 10.0.0.77: seq=2 ttl=64 time=0.134 ms
```

So this worked for a start

### Call the backend webserver
The next test is to call the webserver on the backend using `wget`:

```shell
# wget -qO- http://backend_web-server
afdd6c3aecf1
# wget -qO- http://backend_web-server
afdd6c3aecf1
# wget -qO- http://backend_web-server
afdd6c3aecf1
```
As we can see the hostname returned is always the same one.

### Scale the backend webserver and call again
Now it gets interesting. First I scale the web-server in the backend stack:

```shell
docker service scale backend_web-server=2
```

After the second instance is up and running. I try the wget again:

```shell
# wget -qO- http://backend_web-server
afdd6c3aecf1
# wget -qO- http://backend_web-server
924bafd68751
# wget -qO- http://backend_web-server
afdd6c3aecf1
# wget -qO- http://backend_web-server
924bafd68751
# wget -qO- http://backend_web-server
afdd6c3aecf1
# wget -qO- http://backend_web-server
924bafd68751
```

As we can see the hostname varies indicating the the calls have been `load balanced`.

## Conclustion
* Two connect to stacks in the same swarm an overlay network needs to be created
* Calls from a service from one stack to another one can be done using hostnames like `[stackname]_[service-name]`.
* Requests for a service on the other stack are load balanced
