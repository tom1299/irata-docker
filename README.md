# Docker for IRATA
This repository is a playground for me trying out Docker for the IRATA project.

The main goals are to create small versatile docker images to be used with microservices.

## Current status

* [jahf](./jahf) means _Java_ _Alpine_ _Httpd_ _Filebeat_ and defines an image that contains these three mayor components.
* [jahl](./jahl) means _Java_ _Alpine_ _Httpd_ _Logstash_ similar to the one above only that it contains logstash rather than filebeat.
